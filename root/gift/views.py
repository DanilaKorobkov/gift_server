# Internal
import gift.models as model
# Django
from django.shortcuts import render, get_object_or_404, get_list_or_404

from django.http import HttpResponse


def helloWorld(request):
    return HttpResponse("Hello world.")


def items(request):
    giftItems = get_list_or_404(model.GiftItem)

    context = {'GiftItems': giftItems}
    return render(request, 'gift/items_view.html', context)


def item(request, giftItemId):

    giftItem = get_object_or_404(model.GiftItem, pk = giftItemId)

    context = {'GiftItem': giftItem}
    return render(request, 'gift/item_detail_view.html', context)


