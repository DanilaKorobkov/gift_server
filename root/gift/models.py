# Django
from django.db import models


class GiftItem(models.Model):

    name = models.CharField(max_length = 100, verbose_name = "Название элемента")

    purchasePrice = models.FloatField(verbose_name = "Закупочная цена")
    customerPrice = models.FloatField(verbose_name = "Цена для клиента")

    description = models.TextField(verbose_name = "Описание продукта", default = "")


    def __str__(self):
        return "Элемент: {}, Закупочная цена: {}, Цена для клиента: {}; {}".format(self.name,
                                                                                   self.purchasePrice,
                                                                                   self.customerPrice,
                                                                                   self.description)
