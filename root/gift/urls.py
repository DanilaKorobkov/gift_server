# Internal
from gift.views import *
# Django
from django.urls import path


app_name = 'gift'
urlpatterns = \
[
    path('', helloWorld, name = 'helloWorld'),
    path('items', items, name = 'items'),
    path('item/<int:giftItemId>', item, name = 'item')
]
