# Internal
from .models import GiftItem
# Django
from django.contrib import admin


admin.site.register(GiftItem)
